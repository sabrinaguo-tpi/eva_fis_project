document.writeln("    <header class='header echat_header'>");
document.writeln("      <h2 class='ipd_show'>echat</h2>");
document.writeln(
  "      <!-- 992以下會出現在header的飛機細節面板 flight_infos -->"
);
document.writeln("      <div class='flight_infos ipd_show'>");
document.writeln("        <div class='f_details'>");
document.writeln("          <div class='tags'>");
document.writeln("            <div class='tag delay'>Delay</div>");
document.writeln("            <div class='tag'>Service A</div>");
document.writeln("          </div>");
document.writeln("          <div class='f_No'>");
document.writeln(
  "            <figure><img src='../images/img-eva@2x.png' alt=''></figure>"
);
document.writeln("            <div class='f_No_infos'>");
document.writeln("              <p>EVA Air</p>");
document.writeln("              <h2>BR1234 / B17882 (789)</h2>");
document.writeln(
  "              <p><span class='text-gray'>Act. Date, Code</span><span>2021/10/18, 0</span></p>"
);
document.writeln("            </div>");
document.writeln("          </div>");
document.writeln("        </div>");
document.writeln("        <!-- 動態飛機 -->");
document.writeln("        <div class='flight-detail-place'>");
document.writeln("          <div class='flight-place'>");
document.writeln("            <div class='start'>TPE</div>");
document.writeln("            <div class='time'>02h 42m</div>");
document.writeln("            <div class='end'>TYO</div>");
document.writeln("          </div>");
document.writeln("          <div class='flight-move'>");
document.writeln("            <div class='start'></div>");
document.writeln("            <!-- 起點 width:0%,終點 width:100% -->");
document.writeln(
  "            <div class='ic-flight-line' style='width: 30%'></div>"
);
document.writeln("            <!-- 起點 left:0%,終點 left:100% -->");
document.writeln("            <div class='ic-move-flight' style='left: 30%'>");
document.writeln("              <i class='icon ic-airplane'></i>");
document.writeln("              <p>88h 88m</p>");
document.writeln("            </div>");
document.writeln("            <div class='end'></div>");
document.writeln("          </div>");
document.writeln("        </div>");
document.writeln("      </div>");
document.writeln("    </header>");

/*********************************/
/* 下拉選單 */
/*********************************/

function hiddenPlaceholder($currenSelect) {
  var $isCurrenSelected = $currenSelect.find(".select-single").text();
  if ($isCurrenSelected) {
    $currenSelect.find(".select-placeholder").addClass("d-none");
  }
}

function setSelected(currenSelect) {
  var currenCotent = currenSelect.find(".select-content");
  currenCotent.on("click", ".select-item", function () {
    currenSelect.find(".select-single").text($(this).text());
    currenCotent.children().removeClass("active");
    $(this).addClass("active");
    hiddenPlaceholder($(currenSelect));
  });
}

function bsSelect() {
  var $selects = $(".bs-select");
  $selects.each(function (index) {
    hiddenPlaceholder($(this));
    setSelected($(this));
  });
}
/*********************************/
/* function block  */
/*********************************/
//手機板頁籤切換
function tabClick() {
  if ($(window).width() < 991) {
    $(".btn_SelectAdd").click(function () {
      console.log("dddd");
      $(".tabs a").removeClass("active");
      $(this).addClass("active");
      $(".pos_flex .tab_content").removeClass("active");
      $(".chk_AddressTable").addClass("active");
    });
    $(".btn_PreviewAdd").click(function () {
      console.log("object");
      $(".tabs a").removeClass("active");
      $(this).addClass("active");
      $(".pos_flex .tab_content").removeClass("active");
      $(".show_Address").addClass("active");
    });
  }
}
//100vh計算
var vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty("--vh", `${vh}px`);
//表格中需要特別外連的按鈕，禁止冒泡事件
$(".link_td").click(function (e) {
  e.stopPropagation();
});
//btn_MNUOPENED
$(".btn_MNUOPENED").click(function () {
  $("body,html").toggleClass("opened");
});
//打開 btn_SEARCHBOX 增加黑色遮罩
function openSearchPop(addClassNameObj, className) {
  $(addClassNameObj).addClass(className);
  $("html").css("overflow", "hidden");
}
//關閉 btn_SEARCHBOX 關閉黑色遮罩
function closeSearchPop(addClassNameObj, className) {
  $(addClassNameObj).removeClass(className);
  $("html").css("overflow", "unset");
}
//開啟A-01.1a-Home.html 飛行細節燈箱 A-01.5a Home_quick view_pop
$(".btn_FIGHTDETAILOPEN").click(function (e) {
  $("#pup_flightDetailInfos").modal("show");
  e.stopPropagation();
});
//開啟C-01.2a-AgentScheduleDeparture 飛行細節燈箱 C-01.2b Agent Schedule_departure_quick view_popup
$(".btn_FIGHTDETAILVIEWOPEN").click(function (e) {
  $("#pup_flightDetailInfosView").modal("show");
  e.stopPropagation();
});
//點擊篩選鈕
$(".ic-filter").click(function () {
  $(this).toggleClass("active");
  $(this)
    .parents(".filter-multiple")
    .find(".multiple-options")
    .toggleClass("show");
  if (
    $(this)
      .parents(".filter-multiple")
      .find(".multiple-options")
      .hasClass("single")
  ) {
    $(this)
      .parents(".filter-multiple")
      .find(".form-check")
      .click(function () {
        $(this)
          .parents(".filter-multiple")
          .find(".multiple-options")
          .removeClass("show");
      });
  }
});
// 點空白關 multiple-options
$(document).mouseup(function (e) {
  var container = $(".multiple-options");
  if (!container.is(e.target) && container.has(e.target).length === 0) {
    $(".multiple-options").removeClass("show");
    $(".ic-filter").removeClass("active");
  }
});
//表格下方filter單選的物件 加上active樣式
$(".opt_single .form-check").click(function () {
  $(this).addClass("active").siblings().removeClass("active");
});
// 顯示bootstratp modal時，鎖定背景滑動
$(".modal").on("shown.bs.modal", function () {
  $("html").css("overflow", "hidden");
});
$(".modal").on("hidden.bs.modal", function () {
  $("html").css("overflow", "unset");
});

/*********************************/
/* call function block  */
/*********************************/

$(document).ready(function () {
  bsSelect();
  tabClick();
  var vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);
});
//手機板頁籤切換
$(window).resize(function () {
  tabClick();
  var vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty("--vh", `${vh}px`);
});
/*********************************/
/* echat 聊天室選單  */
/*********************************/
//echat專屬 點擊群組選單 新增active class
$(".mnu_echat .echat_cover .mnu_FlightList .mnu_Lists ").click(function () {
  $(this).addClass("active").siblings().removeClass("active");
  if ($(window).width() < 992) {
    $("body,html").addClass("opened"); //echat專屬 小網使用，選單移出後，展示聊天視窗
  }
});
//echat專屬 小網點擊上一步後，選單移入，遮住聊天視窗
$(".echat_title .btn_GOBACK").click(function () {
  $("body,html").removeClass("opened");
});

/*********************************/
/* 月曆  */
/*********************************/
//日曆 首頁 start ====================================
//每個日曆都需要重新 new
$("#cal_start").daterangepicker({
  autoApply: true,
  singleDatePicker: true,
  timePicker: true,
  timePicker24Hour: true,
  showDropdowns: true,
  minYear: 1901,
  placeholder: "Select a option",
  autoUpdateInput: false,
  parentEl: "#cal_calendar1",
  locale: {
    format: "YYYY/MM/DD hh:mm",
    cancelLabel: "Clear",
  },
});
$("#cal_start").on("apply.daterangepicker", function (ev, picker) {
  $(this).val(picker.startDate.format("YYYY/MM/DD hh:mm"));
});

$("#cal_start").on("cancel.daterangepicker", function (ev, picker) {
  $(this).val("");
});

$("#cal_end").daterangepicker({
  autoApply: true,
  singleDatePicker: true,
  timePicker: true,
  timePicker24Hour: true,
  showDropdowns: true,
  minYear: 1901,
  placeholder: "Select a option",
  autoUpdateInput: false,
  parentEl: "#cal_calendar1",
  locale: {
    format: "YYYY/MM/DD hh:mm",
    cancelLabel: "Clear",
  },
});
$("#cal_end").on("apply.daterangepicker", function (ev, picker) {
  $(this).val(picker.startDate.format("YYYY/MM/DD hh:mm"));
});

$("#cal_end").on("cancel.daterangepicker", function (ev, picker) {
  $(this).val("");
});

//input 輸入格式化套件
var cleave = new Cleave("#cal_start", {
  delimiters: ["/", "/", "  ", ":"],
  blocks: [4, 2, 2, 2, 2],
});
var cleave2 = new Cleave("#cal_end", {
  delimiters: ["/", "/", "  ", ":"],
  blocks: [4, 2, 2, 2, 2],
});

//日曆 首頁 end ======================================

/*********************************/
/* 多選選單  */
/*********************************/

//多選選單 點選後同步更新至選單
//使用方式multipleSelectArr(陣列名稱，event,點擊物件本身)
// multipleSelectArr( m_DepDelayCode,e, $(".m_DepDelayCode .dropdown-menu .form-check-label"));

function multipleSelectArr(arrName, event, $this) {
  if (arrName.indexOf(event.target.innerText) < 0) {
    arrName.push(event.target.innerText);
  } else {
    arrName.splice(arrName.indexOf(event.target.innerText), 1);
  }
  if (arrName.length != 0) {
    $this
      .parents(".btn-dropdown-wrapper")
      .find(".type_text")
      .html("<p>" + arrName + "</p>");
  } else {
    $this
      .parents(".btn-dropdown-wrapper")
      .find(".type_text")
      .html('<p class="text-gray-4">請選擇</p>');
  }
}

//純多選 多選C start ====================================
// 如果同一頁面需要很多複選時，請於 .btn-dropdown 新增classNmae(ex:.selectArr)或自行新增id，且js也需命名新的array名稱(ex:selectArr = [])
//   航班狀態  =========================
var status_opt = [];
$(".status_opt .dropdown-menu .form-check-label").on("click", function (e) {
  e.stopPropagation();
  multipleSelectArr(
    status_opt,
    e,
    $(".status_opt .dropdown-menu .form-check-label")
  );
});

//   m_DepDelayCode  =========================
var m_DepDelayCode = [];
$(".m_DepDelayCode .dropdown-menu .form-check-label").on("click", function (e) {
  e.stopPropagation();
  multipleSelectArr(
    m_DepDelayCode,
    e,
    $(".m_DepDelayCode .dropdown-menu .form-check-label")
  );
});

//   m_ArrDelayCode  =========================
var m_ArrDelayCode = [];
$(".m_ArrDelayCode .dropdown-menu .form-check-label").on("click", function (e) {
  e.stopPropagation();
  multipleSelectArr(
    m_ArrDelayCode,
    e,
    $(".m_ArrDelayCode .dropdown-menu .form-check-label")
  );
});

//   m_AirLine  =========================
var m_AirLine = [];
$(".m_AirLine .dropdown-menu .form-check-label").on("click", function (e) {
  e.stopPropagation();
  multipleSelectArr(
    m_AirLine,
    e,
    $(".m_AirLine .dropdown-menu .form-check-label")
  );
});

//純多選 end ======================================

//多選 + input 多選B start ====================================
// 如果同一頁面需要很多複選時，請於 .btn-dropdown 新增classNmae(ex:.inputSelectArr)或自行新增id，且js也需命名新的array名稱(ex:inputSelectArr = [])
//.ip_search - 選單內的input
$(".ip_search").on("click", function (e) {
  e.stopPropagation();
});

// m_FLT =========================
var m_FLT = [];
$(".m_FLT .dropdown-menu .form-check-label").on("click", function (e) {
  e.stopPropagation();
  multipleSelectArr(m_FLT, e, $(".m_FLT .dropdown-menu .form-check-label"));
});

// m_AC =========================
var m_AC = [];
$(".m_AC .dropdown-menu .form-check-label").on("click", function (e) {
  e.stopPropagation();
  multipleSelectArr(m_AC, e, $(".m_AC .dropdown-menu .form-check-label"));
});

// m_DepStop1 =========================
var m_DepStop1 = [];
$(".m_DepStop1 .dropdown-menu .form-check-label").on("click", function (e) {
  e.stopPropagation();
  multipleSelectArr(
    m_DepStop1,
    e,
    $(".m_DepStop1 .dropdown-menu .form-check-label")
  );
});

// m_ArrStop =========================
var m_ArrStop = [];
$(".m_ArrStop .dropdown-menu .form-check-label").on("click", function (e) {
  e.stopPropagation();
  multipleSelectArr(
    m_ArrStop,
    e,
    $(".m_ArrStop .dropdown-menu .form-check-label")
  );
});

// m_ServiceType =========================
var m_ServiceType = [];
$(".m_ServiceType .dropdown-menu .form-check-label").on("click", function (e) {
  e.stopPropagation();
  multipleSelectArr(
    m_ServiceType,
    e,
    $(".m_ServiceType .dropdown-menu .form-check-label")
  );
});
//多選 + input end ======================================
/*********************************/
/* Fancybox 套件使用  */
/*********************************/
Fancybox.bind('[data-fancybox="gallery"]', {
  infinite: true,
  Toolbar: false,
  closeButton: "top",
  showClass: false,
  hideClass: false,
  Image: {
    zoom: false,
    fit: "contain",
    click: false,
  },
  on: {
    ready: function () {
      $(".btn_DOWNLOAD").show(); //下載按鈕打開
    },
    destroy: function () {
      $(".btn_DOWNLOAD").hide(); //下載按鈕關閉
    },
  },
});

function getAlignedText(text) {
  if (text) {
    text = text.split("\n");
  } else {
    return;
  }
  var searchDisabled = 'disabled=""';
  var searchChecked = 'checked=""';
  while (text.length > 0 && $.trim(text[0]) == "") {
    text.shift();
  }
  var tabs = (text[0] || "").replace(/^(\s+).+$/, "$1");
  for (var i = 0; i < text.length; i++) {
    text[i] = text[i].replace(tabs, "");

    returnData = text[i].indexOf(searchDisabled);
    if (returnData != -1) {
      text[i] = text[i].replace(searchDisabled, "disabled");
    }
    returnData = text[i].indexOf(searchChecked);
    if (returnData != -1) {
      text[i] = text[i].replace(searchChecked, "checked");
    }
  }
  if (text.length > 0 && text[text.length - 1].match(/^\s*$/)) {
    text.pop();
  }
  return text.join("\n");
}

function creatHighlightBlock() {
  $(".demo-block").each(function () {
    var content = $(this).find(".content");
    var controls = $(this).find(".controls");

    $("<code></code>")
      .addClass("lang-html highlight")
      .text(getAlignedText(content.find(".demo").html()))
      .appendTo(content.find(".html pre"));
    $("<code></code>")
      .addClass("ang-js highlight")
      .text(getAlignedText(content.find("script").html()))
      .appendTo(content.find(".js pre"));

    controls.on("click", "span", function () {
      content
        .find("." + $(this).removeClass("active").attr("class"))
        .show()
        .siblings("div")
        .hide();
      $(this).addClass("active").siblings("span").removeClass("active");
    });
    controls.find(".demo").click();
  });
  addHighlight();
}

function addHighlight() {
  document.querySelectorAll(".highlight").forEach(function (block) {
    hljs.highlightBlock(block);
  });
}

/*********************************/
/* call function block  */
/*********************************/

$(document).ready(function () {
  creatHighlightBlock();
});

document.writeln("");
document.writeln(
  "    <nav class='navbar navbar-expand mnu_NavMain mnu_Flight'>"
);
document.writeln(
  "      <!-- 992以上會出現在header的飛機細節面板 flight_infos -->"
);
document.writeln("      <div class='flight_infos pc_show'>");
document.writeln("        <div class='f_details'>");
document.writeln("          <div class='tags'>");
document.writeln("            <div class='tag delay'>Delay</div>");
document.writeln("            <div class='tag'>Service A</div>");
document.writeln("          </div>");
document.writeln("          <div class='f_No'>");
document.writeln(
  "            <figure><img src='../images/img-eva@2x.png' alt=''></figure>"
);
document.writeln("            <div class='f_No_infos'>");
document.writeln("              <p>EVA Air</p>");
document.writeln("              <h2>BR1234 / B17882 (789)</h2>");
document.writeln(
  "              <p><span class='text-gray'>Act. Date, Code</span><span>2021/10/18, 0</span></p>"
);
document.writeln("            </div>");
document.writeln("          </div>");
document.writeln("        </div>");
document.writeln("        <!-- 動態飛機 -->");
document.writeln("        <div class='flight-detail-place'>");
document.writeln("          <div class='flight-place'>");
document.writeln("            <div class='start'>TPE</div>");
document.writeln("            <div class='time'>02h 42m</div>");
document.writeln("            <div class='end'>TYO</div>");
document.writeln("          </div>");
document.writeln("          <div class='flight-move'>");
document.writeln("            <div class='start'></div>");
document.writeln("            <!-- 起點 width:0%,終點 width:100% -->");
document.writeln(
  "            <div class='ic-flight-line' style='width: 30%'></div>"
);
document.writeln("            <!-- 起點 left:0%,終點 left:100% -->");
document.writeln("            <div class='ic-move-flight' style='left: 30%'>");
document.writeln("              <i class='icon ic-airplane'></i>");
document.writeln("              <p>88h 88m</p>");
document.writeln("            </div>");
document.writeln("            <div class='end'></div>");
document.writeln("          </div>");
document.writeln("        </div>");
document.writeln("      </div>");
document.writeln("      <!-- 選單 -->");
document.writeln("      <div class='mnu_flight_cover'>");
document.writeln("        <ul class='mnu_FlightLists'>");
document.writeln("          <li class='mnu_Lists'>");
document.writeln("            <div class='mnu_list_name'>");
document.writeln("              <i class='icon ic-flight'></i>");
document.writeln("              <h2>Flight</h2>");
document.writeln("            </div>");
document.writeln("            <ul class='mnu_sub_lists'>");
document.writeln(
  "              <li class='active'><a href='./B-01.2a-FlightDetail.html'>Flight Detail</a></li>"
);
document.writeln(
  "              <li><a href='./B-02.1a-LoadSheet.html'>Load Sheet</a></li>"
);
document.writeln(
  "              <li><a href='javascript:;'>Booking PAX</a></li>"
);
document.writeln(
  "              <li><a href='javascript:;'>Actual PAX</a></li>"
);
document.writeln(
  "              <li><a href='javascript:;'>Flight Plan</a></li>"
);
document.writeln(
  "              <li><a href='javascript:;'>Baggage List</a></li>"
);
document.writeln(
  "              <li><a href='./B-07.1a-eChat.html' target='_blank'>echat</a></li>"
);
document.writeln("            </ul>");
document.writeln("          </li>");
document.writeln("          <li class='mnu_Lists'>");
document.writeln("            <div class='mnu_list_name'>");
document.writeln("              <i class='icon ic-crew'></i>");
document.writeln("              <h2>Crew</h2>");
document.writeln("            </div>");
document.writeln("            <ul class='mnu_sub_lists'>");
document.writeln(
  "              <li><a href='./B-08.1a-CrewList.html'>Crew List</a></li>"
);
document.writeln("            </ul>");
document.writeln("          </li>");
document.writeln("          <li class='mnu_Lists'>");
document.writeln("            <div class='mnu_list_name'>");
document.writeln("              <i class='icon ic-dispatch'></i>");
document.writeln("              <h2>Dispatch</h2>");
document.writeln("            </div>");
document.writeln("            <ul class='mnu_sub_lists'>");
document.writeln(
  "              <li><a href='./B-09.1a-TelexList.html'>Telex List</a></li>"
);
document.writeln(
  "              <li><a href='./B-10.1a-Uplink.html'>Uplink</a></li>"
);
document.writeln(
  "              <li><a href='./B-11.1a-Send.html'>Send</a></li>"
);
document.writeln("            </ul>");
document.writeln("          </li>");
document.writeln("          <li class='mnu_Lists'>");
document.writeln("            <div class='mnu_list_name'>");
document.writeln("              <i class='icon ic-telex'></i>");
document.writeln("              <h2>Format Telex</h2>");
document.writeln("            </div>");
document.writeln("            <ul class='mnu_sub_lists'>");
document.writeln(
  "              <li><a href='./B-12.1a-MAD(Blockout).html'>MAD</a></li>"
);
document.writeln("              <li><a href='javascript:;'>MAA</a></li>");
document.writeln("              <li><a href='javascript:;'>MRR</a></li>");
document.writeln("              <li><a href='javascript:;'>MEA</a></li>");
document.writeln("              <li><a href='javascript:;'>MED</a></li>");
document.writeln("              <li><a href='javascript:;'>MFR</a></li>");
document.writeln(
  "              <li><a href='./B-18.1b-MMM(24hr).html'>MMM</a></li>"
);
document.writeln("            </ul>");
document.writeln("          </li>");
document.writeln("          <li class='mnu_Lists'>");
document.writeln("            <div class='mnu_list_name'>");
document.writeln("              <i class='icon ic-aircraft'></i>");
document.writeln("              <h2>Aircraft</h2>");
document.writeln("            </div>");
document.writeln("            <ul class='mnu_sub_lists'>");
document.writeln(
  "              <li><a href='./B-19.1a-AircraftList.html'>Aircraft List</a></li>"
);
document.writeln(
  "              <li><a href='./B-20.1a-Recent.html'>Recent</a></li>"
);
document.writeln(
  "              <li><a href='./B-21.1a-AircraftStatus.html'>Aircraft Status</a></li>"
);
document.writeln("              <li><a href='./B-22.1a-MEL.html'>MEL</a></li>");
document.writeln(
  "              <li><a href='./B-23.1a-ACMS.html'>ACMS / CMC</a></li>"
);
document.writeln("            </ul>");
document.writeln("          </li>");
document.writeln("          <li class='mnu_Lists'>");
document.writeln("            <div class='mnu_list_name'>");
document.writeln("              <i class='icon ic-log'></i>");
document.writeln("              <h2>Electronic Planning Log</h2>");
document.writeln("            </div>");
document.writeln("            <ul class='mnu_sub_lists'>");
document.writeln(
  "              <li><a href='./B-24.1a-DispatchRelease.html'>Dispatch Release</a></li>"
);
document.writeln("              <li><a href='./B-25.1a-OFP.html'>OFP</a></li>");
document.writeln("            </ul>");
document.writeln("          </li>");
document.writeln("        </ul>");
document.writeln("      </div>");
document.writeln("    </nav>");

//開啟pop，請於開啟pop的按鈕新增點擊事件
//onclick="pop_open('欲開啟的燈箱id')"
//若要pop內在開pop，須在按鈕上增加  data-open="true"
//可參考 B-18.4b-MMM_TR_table.html #pup_createInfos的pop
$("[data-dismiss]").click(function () {
  $("body").removeClass("open_pop");
  if ($(this).data("open")) {
    $("body").addClass("open_pop");
  }
});
function pop_open(pop_name) {
  $(pop_name).modal();
  $(".modal-backdrop").appendTo(".flightDetail_content");
  $("body").removeClass().addClass("open_pop");
}
